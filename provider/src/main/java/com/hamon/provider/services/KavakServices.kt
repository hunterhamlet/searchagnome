package com.hamon.provider.services

import com.hamon.provider.model.KavakServiceResponse
import com.hamon.provider.utils.URLServices
import retrofit2.Response
import retrofit2.http.GET

interface KavakServices {
    @GET(URLServices.GET_LIST_POPULATION)
    suspend fun getListOfPopulation(): Response<KavakServiceResponse>
}