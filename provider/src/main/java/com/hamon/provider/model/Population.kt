package com.hamon.provider.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Population (
    @SerializedName("id") val id: Int = 0,
    @SerializedName("name") val name: String = "",
    @SerializedName("thumbnail") val thumbnail: String = "",
    @SerializedName("age") val age: Int = 0,
    @SerializedName("weight") val weight: Double = 0.0,
    @SerializedName("height") val height: Double = 0.0,
    @SerializedName("hair_color") val hairColor: String = "",
    @SerializedName("professions") val professions: MutableList<String> = mutableListOf(),
    @SerializedName("friends") val friends: MutableList<String> = mutableListOf(),
): Serializable