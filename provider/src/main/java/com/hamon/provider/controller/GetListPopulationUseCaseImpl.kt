package com.hamon.provider.controller

import com.hamon.provider.actions.APIActions
import com.hamon.provider.repository.RemoteDataSource

class GetListPopulationUseCaseImpl(private val remoteDataSource: RemoteDataSource) :
    GetListPopulationUseCase {
    override suspend fun invoke(): APIActions {
        return remoteDataSource.getPopulationList()
    }
}

interface GetListPopulationUseCase {
    suspend fun invoke(): APIActions
}