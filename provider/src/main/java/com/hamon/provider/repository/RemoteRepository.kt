package com.hamon.provider.repository

import com.hamon.provider.API
import com.hamon.provider.actions.APIActions
import com.hamon.provider.services.KavakServices
import timber.log.Timber
import java.lang.Exception

class RemoteRepository : RemoteDataSource {

    private val api = API.getServices<KavakServices>()

    override suspend fun getPopulationList(): APIActions {
        return try {
            val response = api.getListOfPopulation()
            if (response.isSuccessful) {
                response.body()?.let { serviceResponse ->
                    APIActions.OnSuccess(response = serviceResponse)
                } ?: run {
                    APIActions.OnError(message = "Ups, tenemos problemas. Intentelo más tarde.")
                }
            } else {
                APIActions.OnError(message = "Ups, tenemos problemas. Intentelo más tarde.")
            }
        } catch (exception: Exception) {
            Timber.e("An error has in RemoteRepository -> fun getPopulationList")
            APIActions.OnError(message = "Ups, tenemos problemas. Intentelo más tarde.")
        }
    }
}

interface RemoteDataSource {
    suspend fun getPopulationList(): APIActions
}