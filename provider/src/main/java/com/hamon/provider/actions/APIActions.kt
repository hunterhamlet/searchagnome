package com.hamon.provider.actions

sealed class APIActions {
    data class OnSuccess<T>(val response: T):APIActions()
    data class OnError(val message: String): APIActions()
}