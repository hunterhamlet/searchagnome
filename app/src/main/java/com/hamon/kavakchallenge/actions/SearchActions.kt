package com.hamon.kavakchallenge.actions

import com.hamon.provider.model.Population

sealed class SearchActions {
    object Init: SearchActions()
    data class HasData(val productList: MutableList<Population>): SearchActions()
    object EmptySearch: SearchActions()
    data class HasError(val message: String): SearchActions()
}