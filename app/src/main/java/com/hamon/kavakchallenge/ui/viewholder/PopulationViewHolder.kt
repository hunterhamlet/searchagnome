package com.hamon.kavakchallenge.ui.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.hamon.kavakchallenge.databinding.LayoutItemPopulationBinding
import com.hamon.kavakchallenge.util.USER_AGENT
import com.hamon.kavakchallenge.util.load
import com.hamon.provider.model.Population

class PopulationViewHolder(private val binding: LayoutItemPopulationBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(population: Population, onClicked: (population: Population) -> Unit) {
        binding.apply {
            ivPhotoPopulation.load(population.thumbnail)
            tvNamePopulation.text = population.name
            cPopulation.setOnClickListener {
                onClicked.invoke(population)
            }
        }
    }

}