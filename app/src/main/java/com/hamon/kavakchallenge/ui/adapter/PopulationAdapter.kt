package com.hamon.kavakchallenge.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hamon.kavakchallenge.databinding.LayoutItemPopulationBinding
import com.hamon.kavakchallenge.ui.viewholder.PopulationViewHolder
import com.hamon.provider.model.Population

class PopulationAdapter(
    private val populationList: MutableList<Population> = mutableListOf(),
    private val onClicked: (population: Population) -> Unit,
) : RecyclerView.Adapter<PopulationViewHolder>() {

    fun submitList(newList: MutableList<Population>){
        populationList.clear()
        populationList.addAll(newList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopulationViewHolder =
        PopulationViewHolder(
            LayoutItemPopulationBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )


    override fun onBindViewHolder(holder: PopulationViewHolder, position: Int) {
        holder.bind(populationList[position], onClicked)
    }

    override fun getItemCount(): Int = populationList.size
}