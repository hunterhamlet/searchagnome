package com.hamon.kavakchallenge.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hamon.kavakchallenge.databinding.LayoutItemStringBinding
import com.hamon.kavakchallenge.ui.viewholder.StringViewHolder

class StringAdapter(private val stringList: MutableList<String> = mutableListOf()) :
    RecyclerView.Adapter<StringViewHolder>() {

    fun submit(newList: MutableList<String>){
        stringList.clear()
        stringList.addAll(newList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringViewHolder =
        StringViewHolder(
            LayoutItemStringBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )


    override fun onBindViewHolder(holder: StringViewHolder, position: Int) {
        holder.bind(stringList[position])
    }

    override fun getItemCount(): Int = stringList.size
}