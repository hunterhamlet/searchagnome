package com.hamon.kavakchallenge.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.hamon.kavakchallenge.R
import com.hamon.kavakchallenge.base.BaseFragment
import com.hamon.kavakchallenge.databinding.FragmentDetailSearchBinding
import com.hamon.kavakchallenge.ui.adapter.StringAdapter
import com.hamon.kavakchallenge.util.load
import com.hamon.kavakchallenge.viewmodel.MainViewModel
import com.hamon.kavakchallenge.viewmodel.ViewModelFactory
import com.hamon.provider.model.Population
import org.koin.android.ext.android.inject

class DetailSearchFragment : BaseFragment() {

    private val binding: FragmentDetailSearchBinding by lazy {
        FragmentDetailSearchBinding.inflate(LayoutInflater.from(context), null, false)
    }
    private val viewModelFactory: ViewModelFactory by inject()
    private val viewModel: MainViewModel by navGraphViewModels(R.id.nav_graph) {
        viewModelFactory
    }
    private val friendAdapter: StringAdapter by lazy { StringAdapter() }
    private val professionAdapter: StringAdapter by lazy { StringAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservable()
    }

    override fun onBackPressFunction() {
        super.onBackPressFunction()
        findNavController().navigateUp()
    }

    private fun setupObservable() {
        viewModel.populationSelected.observerInside { population ->
            setupUI(population)
        }
    }

    private fun setupRecyclerView() {
        binding.apply {
            rvActivities.adapter = professionAdapter
            rvFriend.adapter = friendAdapter
        }
    }

    private fun setupUI(population: Population) {
        friendAdapter.submit(population.friends)
        professionAdapter.submit(population.professions)
        binding.apply {
            ivPhoto.load(population.thumbnail)
            tvName.text = population.name
            tvWeight.text = getString(R.string.weight_string, population.weight.toString())
            tvHeight.text = getString(R.string.height_string, population.height.toString())
            tvAge.text = getString(R.string.age_string, population.age.toString())
            tvHairColor.text = getString(R.string.hair_color_string, population.hairColor)
            cPopulationInfo.visibility = View.VISIBLE
            tvName.text = population.name
            if (population.friends.size != 0) {
                tvZeroFriends.visibility = View.GONE
                rvFriend.visibility = View.VISIBLE
            }
            if (population.professions.size != 0) {
                tvZeroProfession.visibility = View.GONE
                rvActivities.visibility = View.VISIBLE
            }
        }
        hideProgressBar()
    }

}