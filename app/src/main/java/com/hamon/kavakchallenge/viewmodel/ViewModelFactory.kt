package com.hamon.kavakchallenge.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.hamon.provider.controller.GetListPopulationUseCaseImpl

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(
    private val application: Application,
    private val getListPopulationUseCaseImpl: GetListPopulationUseCaseImpl
) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return with(modelClass) {
            when {
                isAssignableFrom(MainViewModel::class.java) -> MainViewModel(
                    application,
                    getListPopulationUseCaseImpl
                )
                else -> throw IllegalArgumentException("Unknown ViewModel class you must add it") as Throwable
            }
        } as T
    }

}