package com.hamon.kavakchallenge.viewmodel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hamon.kavakchallenge.actions.NetworkActions
import com.hamon.kavakchallenge.actions.SearchActions
import com.hamon.kavakchallenge.base.BaseApplicationViewModel
import com.hamon.provider.actions.APIActions
import com.hamon.provider.controller.GetListPopulationUseCaseImpl
import com.hamon.provider.model.KavakServiceResponse
import com.hamon.provider.model.Population
import kotlinx.coroutines.launch
import java.util.*

class MainViewModel(
    private val app: Application,
    private val getListPopulationUseCaseImpl: GetListPopulationUseCaseImpl
) :
    BaseApplicationViewModel(app) {

    private val _searchActionsObservable: MutableLiveData<SearchActions> by lazy {
        MutableLiveData<SearchActions>().apply { value = SearchActions.Init }
    }
    val searchActionsObservable: LiveData<SearchActions> get() = _searchActionsObservable
    private val _networkActionsObservable: MutableLiveData<NetworkActions> by lazy {
        MutableLiveData<NetworkActions>().apply { value = NetworkActions.Init }
    }
    val networkActionsObservable: LiveData<NetworkActions> get() = _networkActionsObservable
    private val _listPopulationCache: MutableList<Population> = mutableListOf()
    private val _searchQuery: MutableLiveData<String> by lazy {
        MutableLiveData<String>().apply { value = "" }
    }
    private val _populationSelected: MutableLiveData<Population> by lazy {
        MutableLiveData<Population>().apply { value = Population() }
    }
    val populationSelected: LiveData<Population> get() = _populationSelected

    init {
        getPopulationList()
    }

    private fun getPopulationList() {
        viewModelScope.launch {
            when (val result = getListPopulationUseCaseImpl.invoke()) {
                is APIActions.OnSuccess<*> -> onSuccessPopulationList((result.response as KavakServiceResponse).response)
                is APIActions.OnError -> _searchActionsObservable.postValue(
                    SearchActions.HasError(
                        message = result.message
                    )
                )
            }
        }
    }

    fun setQuery(query: String) {
        _searchQuery.value = query
    }

    fun searchPopulation() {
        _searchQuery.value?.let { query ->
            val listFilter = _listPopulationCache.filter { population ->
                population.name.contains(
                    query,
                    ignoreCase = true
                ) || population.professions.contains(query)
            }
            if (listFilter.isEmpty()) {
                _searchActionsObservable.postValue(SearchActions.EmptySearch)
            } else {
                _searchActionsObservable.postValue(SearchActions.HasData(listFilter.toMutableList()))
            }
        }
    }


    private fun onSuccessPopulationList(populationList: MutableList<Population>) {
        _listPopulationCache.addAll(populationList)
        _searchActionsObservable.postValue(
            SearchActions.HasData(populationList)
        )
    }

    fun populationSelected(population: Population){
        _populationSelected.postValue(population)
    }

    fun restartNetworkEvent() {
        _networkActionsObservable.value = NetworkActions.Init
    }

    fun isNetworkConnected() {
        val connectivityManager =
            app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            checkConnectivityInAndroidPOrBelow(connectivityManager)
        } else {
            checkConnectivityInAndroidPOrHigher(connectivityManager)
        }
    }

    private fun checkConnectivityInAndroidPOrBelow(connectivityManager: ConnectivityManager) {
        if (connectivityManager.activeNetworkInfo?.isConnectedOrConnecting == true) {
            _networkActionsObservable.postValue(NetworkActions.NetworkOK)
        } else {
            _networkActionsObservable.postValue(NetworkActions.NetworkError)
        }
    }

    private fun checkConnectivityInAndroidPOrHigher(connectivityManager: ConnectivityManager) {
        connectivityManager.registerDefaultNetworkCallback(object :
            ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                _networkActionsObservable.postValue(NetworkActions.NetworkOK)
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                _networkActionsObservable.postValue(NetworkActions.NetworkError)
            }
        })
    }


}